# ACP controle times

Current version editor: Toby Wong, chunyauw @ uoregon.edu

## Description

This programme and web-based interface is based upton the ACP controle times.

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders).

An example of a model based upon, would be the following calculator (https://rusa.org/octime_acp.html).

Differences between this and proj5-mongo, is we further implement methods or api_s to extract information out of mongodb.

## How to use

Change your directory into ./DockerRestAPI

Either execute $docker-compose up , or $./run.sh as a provided shell script file.

Use your browser for the below functionalities

Examples after docker-compose:
* goto "http://localhost:5001/"
* register for account, both username and passwords are at least 4 characters. (screen will jump to json response if success)
* goback to homepage, and login using registered accounts. (screen will jump to json response if success)
* now you will be able to access functions such as "http://localhost:5001/listAll"

## Functionality

This project has following four parts. Change the values for host and port according to your machine, and use the web browser to check the results.

* "http://localhost:5001/" should be the homepage for the user interface of signup, login, and log out.

* "http://localhost:5002/" should be the homepage for the user interface of ACP controle times calculator

* "http://localhost:5000/" should be a homepage of an example of implementing the below-mentioned functionalities.

* "http://localhost:5001/listAll" should return all open and close times in the database
* "http://localhost:5001/listOpenOnly" should return open times only
* "http://localhost:5001/listCloseOnly" should return close times only
* "http://localhost:5001/listAll/csv" should return all open and close times in CSV format
* "http://localhost:5001/listOpenOnly/csv" should return open times only in CSV format
* "http://localhost:5001/listCloseOnly/csv" should return close times only in CSV format
* "http://localhost:5001/listAll/json" should return all open and close times in JSON format
* "http://localhost:5001/listOpenOnly/json" should return open times only in JSON format
* "http://localhost:5001/listCloseOnly/json" should return close times only in JSON format
* "http://localhost:5001/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
* "http://localhost:5001/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
* "http://localhost:5001/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
* "http://localhost:5001/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format


There will also be extra 2 parts.

### Part 1: Authenticating the services 

- POST **/api/register**

Registers a new user. On success a status code 201 is returned. The body of the response contains a JSON object with the newly added user. A `Location` header contains the URI of the new user. On failure status code 400 (bad request) is returned. Note: The password is hashed before it is stored in the database. Once hashed, the original password is discarded. Your database should have three fields: id (unique index), username and password for storing the credentials.

- GET **/api/token**

Returns a token. This request must be authenticated using a HTTP Basic Authentication (see password.py for example). On success a JSON object is returned with a field `token` set to the authentication token for the user and a field `duration` set to the (approximate) number of seconds the token is valid. On failure status code 401 (unauthorized) is returned.

- GET **/RESOURCE-YOU-CREATED-IN-PROJECT-6**

Return a protected <resource>, which is basically what you created in project 6. This request must be authenticated using token-based authentication only (see testToken.py). HTTP password-based (basic) authentication is not allowed. On success a JSON object with data for the authenticated user is returned. On failure status code 401 (unauthorized) is returned.

### Part 2: User interface

The goal of this part of the project is to create frontend/UI for Brevet app using Flask-WTF and Flask-Login introduced in lectures. You frontend/UI should use the authentication that you created above. In addition to creating UI for basic authentication and token generation, you will add three additional functionalities in your UI: (a) remember me, (b) logout, and (c) CSRF protection. Note: You don’t have to maintain sessions.

## Tasks

You'll turn in your credentials.ini using which we will get the following:

* The working application with two parts.

* Dockerfile

* docker-compose.yml

## Grading Rubric

* If your code works as expected: 100 points. This includes:
    * Basic APIs work as expected in part 1.
    * Decent user interface in part 2 including three functionalities in the UI.

* For each non-working API in part 1, 15 points will be docked off. Part 1 carries 45 points.

* For the UI and the three functionalies, decent UI carries 15 points. Each functionality carries 10 points. In short, part 2 carries 45 points.

* If none of them work, you'll get 10 points assuming
    * README is updated with your name and email ID.
    * The credentials.ini is submitted with the correct URL of your repo.
    * Dockerfile is present. 
    * Docker-compose.yml works/builds without any errors.

* If the Docker-compose.yml doesn't build or if credentials.ini is missing, 0 will be assigned.
