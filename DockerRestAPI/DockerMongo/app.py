import os
import flask
import arrow
import acp_times
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient

app = Flask(__name__)

client = MongoClient('db', 27017)
db = client.tododb
db.tododb.delete_many({})


###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    return flask.render_template('404.html'), 404


@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km

    begin_datetime = request.args.get('begin_date', "2017-01-01", type=str) + ' ' + request.args.get('begin_time', "00:00", type=str) + '-08:00'
    begin_arrow = arrow.get(begin_datetime, "YYYY-MM-DD hh:mmZZ")
    brevets_distance = request.args.get('brevet_dist_km', 200, type=float)

    open_time = acp_times.open_time(km, brevets_distance, begin_arrow.isoformat())
    close_time = acp_times.close_time(km, brevets_distance, begin_arrow.isoformat())
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


@app.route('/display', methods=['POST'])
def display():
    items = [item for item in db.tododb.find()]
    if (len(items) == 0):
        return render_template('dserror.html')
    return render_template('display.html', items=items)

@app.route('/submit', methods=['POST'])
def submit():
    _open = request.form.getlist("open")
    _close = request.form.getlist("close")
    _dist = request.form.getlist("km")
    toStringOpen = []
    toStringClose = []
    toStringDist = []
    for item in _open:
        if (str(item) != ""): toStringOpen.append(str(item))
    for item in _close:
        if (str(item) != ""): toStringClose.append(str(item))
    for item in _dist:
        if (str(item) != ""): toStringDist.append(str(item))
    length = max(len(toStringOpen), len(toStringClose), len(toStringDist))
    if length == 0:
        return render_template('dserror.html')
    for i in range(length):
        item_doc = {
            '_open': toStringOpen[i],
            '_close': toStringClose[i],
            '_dist': toStringDist[i]
        }
        db.tododb.insert_one(item_doc)
    return redirect(url_for('index'))

#############

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug = True)
