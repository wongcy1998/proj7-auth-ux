import flask
import csv
import json
from flask import Flask, Response, request, render_template
from flask_restful import Resource, Api, reqparse
import logging
import pymongo
from pymongo import MongoClient
from base64 import b64decode
from bson.objectid import ObjectId
from password import hash_password, verify_password
from testToken import generate_auth_token, verify_auth_token, token_required
from flask_login import LoginManager,login_user,logout_user,login_required
from flask_wtf import FlaskForm, CSRFProtect
from wtforms import Form, StringField,PasswordField, BooleanField,validators
from wtforms.validators import InputRequired, Length


app = Flask(__name__)
api = Api(app)
app.config['SECRET_KEY'] = "YajuuSenpai1145141919810"
client = MongoClient("db", 27017)
db = client.tododb
collection = db.control
login_manager = LoginManager()
login_manager.init_app(app)
csrf = CSRFProtect(app)
csrf_token = "1145141919810yajuusenpai"

class User():
	def __init__(self, user_id):
		self.user_id = user_id

	def is_authenticated(self):
		return True
	def is_active(self):
		return True
	def is_anonymous(self):
		return False
	def get_id(self):
		return self.user_id

@login_manager.user_loader
def load_user(user_id):
	ID = str(user_id)
	user = collection.users.find_one(ObjectId(ID))
	app.logger.debug(user)
	if user == None:
		return None
	return User(user['_id'])

class LoginForm(FlaskForm):
	username = StringField('username', validators=[InputRequired(), Length(min=4, max=15)])
	password = PasswordField('password', validators=[InputRequired(), Length(min=4, max=80)])
	remember = BooleanField('remember me')

class SignupForm(FlaskForm):
	username = StringField('username', validators=[InputRequired(), Length(min=4, max=15)])
	password = PasswordField('password', validators=[InputRequired(), Length(min=4, max=80)])

@app.route('/')
def index():
	return render_template('index.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
	form = LoginForm()
	username = form.username.data
	password = form.password.data
	remember = form.remember.data

	if request.method == 'POST' and form.validate_on_submit():
		user = collection.users.find_one({"username":username})
		if user == None:
			flask.flash("Unauthorized, user not found")
			return render_template('login.html', form=form)
		if not verify_password(password, user['password']):
			flask.flash("Unauthorized, wrong password")
			return render_template('login.html', form=form)

		userID = str(user['_id'])
		user_obj = User(userID)
		login_user(user_obj, remember=remember)
		token = generate_auth_token(expiration=1000)
		return flask.jsonify({"token":token.decode(), "duration":1000}), 200
	return render_template('login.html', form=form)

@app.route('/logout')
@login_required
def logout():
	logout_user()
	return "User logged out."

@app.route('/signup', methods=['GET','POST'])
def signup():
	form = SignupForm()
	if form.validate_on_submit():
		username = form.username.data
		password = form.password.data

		if username == None or password == None:
			flask.flash("empty fields!")
			return render_template('signup.html', form=form)

		if collection.users.find_one({"username":username}) != None:
			flask.flash("username already exists")
			return render_template('signup.html', form=form)

		hashedpass = hash_password(password)
		password = None

		post_id = collection.users.insert_one({"username":username,"password":hashedpass})
		userId = str(post_id.inserted_id)

		return flask.jsonify({"success":"created","username":username,"location":userId}),200

	return render_template('signup.html', form=form)

class registerUser(Resource):
	def post(self):
		parser = reqparse.RequestParser()
		parser.add_argument('username', required=True, help="username cannot be blank!")
		parser.add_argument('password', required=True, help="password cannot be blank")
		args = parser.parse_args()

		username = args['username']
		password = args['password']

		if username == None or password == None:
			return {"client error":"bad request"}, 400

		if collection.users.find_one({"username":username}) != None:
			return {"client error":"username already exists"}, 418
		hashedpass = hash_password(password)
		password = None

		post_id = collection.users.insert_one({"username":username,"password":hashedpass})
		userId = str(post_id.inserted_id)

		return {"success":"created","username":username,"location":userId}, 201

class getToken(Resource):
	def get(self):
		authheader = request.headers.get("Authorization")
		if authheader == None:
			return {"Unauthorized":"No Authorization header found"}, 401

		credentials = authheader.split(' ')
		decode_creds = b64decode(credentials[1]).decode()
		user = decode_creds.split(':')
		username = user[0]
		password = user[1]
		
		document = collection.users.find_one({"username":username})
		if document == None:
			return {"Unauthorized":"user not found"}, 401

		if not verify_password(password, document['password']):
			return {"Unauthorized":"wrong password"}, 401

		token = generate_auth_token(expiration=1000)
		return {"token":token.decode(), "duration":1000}, 200


class _listAll(Resource):
    @login_required
    def get(self):
        top = request.args.get("top")
        if top == None:
            top = 1024
        db_finder = db.tododb.find().sort("_open_time", 1).limit(int(top))
        full_db = [i for i in db_finder]
        return {
            '_open_time': [i['_open'] for i in full_db],
            '_close_time': [i['_close'] for i in full_db]
        }

class _listAll_JSON(Resource):
    @login_required
    def get(self):
        top = request.args.get("top")
        if top == None:
            top = 1024
        db_finder = db.tododb.find().sort("_open_time", 1).limit(int(top))
        full_db = [i for i in db_finder]
        return {
            '_open_time': [i['_open'] for i in full_db],
            '_close_time': [i['_close'] for i in full_db]
        }

class _listAll_CSV(Resource):
    @login_required
    def get(self):
        top = request.args.get("top")
        if top == None:
            top = 1024
        db_finder = db.tododb.find().sort("_open_time", 1).limit(int(top))
        full_db = [i for i in db_finder]
        csv = ""
        for i in full_db:
            csv += i['_open'] + ', ' + i['_close'] + ', '
        return csv

class _listOpen(Resource):
    @login_required
    def get(self):
        top = request.args.get("top")
        if top == None:
            top = 1024
        db_finder = db.tododb.find().sort("_open_time", 1).limit(int(top))
        return {
            '_open_time': [i['_open'] for i in db_finder]
        }

class _listOpen_JSON(Resource):
    @login_required
    def get(self):
        top = request.args.get("top")
        if top == None:
            top = 1024
        db_finder = db.tododb.find().sort("_open_time", 1).limit(int(top))
        return {
            '_open_time': [i['_open'] for i in db_finder]
        }

class _listOpen_CSV(Resource):
    @login_required
    def get(self):
        top = request.args.get("top")
        if top == None:
            top = 1024
        db_finder = db.tododb.find().sort("_open_time", 1).limit(int(top))
        full_db = [i for i in db_finder]
        csv = ""
        for i in full_db:
            csv += i['_open'] + ', '
        return csv

class _listClose(Resource):
    @login_required
    def get(self):
        top = request.args.get("top")
        if top == None:
            top = 1024
        db_finder = db.tododb.find().sort("_close_time", 1).limit(int(top))
        return {
            '_close_time': [i['_close'] for i in db_finder]
        }

class _listClose_JSON(Resource):
    @login_required
    def get(self):
        top = request.args.get("top")
        if top == None:
            top = 1024
        db_finder = db.tododb.find().sort("_close_time", 1).limit(int(top))
        return {
            '_close_time': [i['_close'] for i in db_finder]
        }

class _listClose_CSV(Resource):
    @login_required
    def get(self):
        top = request.args.get("top")
        if top == None:
            top = 1024
        db_finder = db.tododb.find().sort("_close_time", 1).limit(int(top))
        full_db = [i for i in db_finder]
        csv = ""
        for i in full_db:
            csv += i['_close'] + ', '
        return csv

api.add_resource(_listAll, '/listAll')
api.add_resource(_listAll_JSON, '/listAll/json')
api.add_resource(_listAll_CSV, '/listAll/csv')
api.add_resource(_listOpen, '/listOpenOnly')
api.add_resource(_listOpen_JSON, '/listOpenOnly/json')
api.add_resource(_listOpen_CSV, '/listOpenOnly/csv')
api.add_resource(_listClose, '/listCloseOnly')
api.add_resource(_listClose_JSON, '/listCloseOnly/json')
api.add_resource(_listClose_CSV, '/listCloseOnly/csv')
api.add_resource(registerUser, '/api/register')
api.add_resource(getToken, '/api/token')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
